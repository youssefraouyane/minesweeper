#include "timerlabel.h"

TimerLabel::TimerLabel(QWidget * parent):QLabel(parent),counter(0)
{
    setStyleSheet("font: 81 15pt \"Rockwell Extra Bold\";");
    setText("000");
    mTimer = new QTimer(this);
    connect(mTimer, SIGNAL(timeout()), this, SLOT(incrementCounter()));
}

TimerLabel::~TimerLabel()
{

}
void TimerLabel::resetTimer()
{
    mTimer->stop();
    counter=0;
    setText("000");
}

void TimerLabel::startTimer()
{
    mTimer->start(1000);
}

void TimerLabel::incrementCounter()
{
    counter++;
    if(counter<=999)
        setText(QString::number(counter).rightJustified(3, '0'));
}

void TimerLabel::stopTimer()
{
    mTimer->stop();
}
