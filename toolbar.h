#ifndef TOOLBAR_H
#define TOOLBAR_H

#include <QObject>
#include <QToolBar>
#include <QAction>
#include <QLabel>
#include <QDebug>
#include "timerlabel.h"

class ToolBar: public QToolBar
{
    Q_OBJECT
    public:
        ToolBar(QWidget * parent = nullptr);
        ~ToolBar();
        inline QAction * getResetAction()const {return mReset;}
        inline QLabel * getSkullFaceLabel()const {return mSkullFace;}
        inline QLabel * getRemainingBombsLabel()const {return mRemainingBombs;}
        inline QLabel * getClockLabel()const {return mClock;}
        inline TimerLabel * getTimerLabel()const {return mTimer;}

    public slots:
        void updateRemainingBombs(bool);

    private:
        QAction * mReset;
        QLabel * mSkullFace;
        QLabel * mRemainingBombs;
        QLabel * mClock;
        TimerLabel * mTimer;

        void initializeActions();
        void addActionsToToolbar();
};

#endif // TOOLBAR_H
