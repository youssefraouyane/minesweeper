#ifndef MENUBAR_H
#define MENUBAR_H

#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QActionGroup>
#include "mainwindow.h"

class MainWindow;
class MenuBar:public QMenuBar
{
    Q_OBJECT
    public:
        MenuBar(QWidget *parent = nullptr);
        ~MenuBar();
        inline QAction * getNewGameAction()const {return newGame;}
        inline QAction * getHallOfFameAction()const {return hallOfFame;}
        inline QAction * getEasyModeAction()const {return easyMode;}
        inline QAction * getMediumModeAction()const {return mediumMode;}
        inline QAction * getHardModeAction()const {return hardMode;}
        inline QAction * getCloseGameAction() const {return closeGame;}
        inline QActionGroup * getDifficultiesActionGroup()const {return mDifficultiesActionGroup;}
        inline QMenu * getDifficultiesMenu()const{return mDifficultiesMenu;}

    private:
        QMenu * generalMenu;
        QAction * newGame;
        QAction * hallOfFame;
        QAction * easyMode;
        QAction * mediumMode;
        QAction * hardMode;
        QAction * closeGame;
        QActionGroup * mDifficultiesActionGroup;
        QMenu * mDifficultiesMenu;

        void initializeActions();
        void addActionsToTheMenu();

};

#endif // MENUBAR_H
