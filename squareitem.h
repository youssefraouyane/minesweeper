#ifndef SQUAREITEM_H
#define SQUAREITEM_H

#include <QWidget>
#include <QPushButton>
#include <QMouseEvent>
#include <QDebug>
#include <QIcon>
#include <QPixmap>
class SquareItem : public QPushButton
{
    Q_OBJECT
    public:
        SquareItem(QWidget *parent = nullptr);
        SquareItem(int p_xpos, int p_ypos, int p_height, int p_length, bool p_isbomb, int p_associatedBombs,bool p_isMarkedAsABomb,
                   QWidget *parent = nullptr, int fontSize = 12);
        SquareItem(int p_xpos, int p_ypos, int p_height, int p_length, QWidget *parent = nullptr, int fontSize=12);
        ~SquareItem();
        void showButton();
        void setAsBomb();
        void setAssociatedBombs(int p_associatedBombs);
        void changeMarkedIcon();
        static bool firstClick;
        inline bool getIsBomb()const {return isBomb;}
        inline int getAssociatedBombs()const {return associatedBombs;}
        inline bool getIsMarkedAsBomb()const {return isMarkedAsABomb;}

    public slots:
        void setIsMarKedAsABomb();

    private slots:
        void mousePressEvent(QMouseEvent *e);
        //void mouseMoveEvent(QMouseEvent *event);

    signals:
        void signalfirstClick(int x_pos, int y_pos);
        void buttonClicked(int x_pos, int y_pos);
        void bombPressed(int x_pos, int y_pos);
        void freeNeighborhood(int x_pos, int y_pos);
        void signalIsMarkedAsABomb(bool);

    private:
        bool isBomb;
        int associatedBombs;
        bool isMarkedAsABomb;
        int xpos;
        int ypos;
        QString getAssociatedColor();
        int mFontSize;
};


#endif // SQUAREITEM_H
