#ifndef GAMEBOARD_H
#define GAMEBOARD_H

#include <QObject>
#include <QVector>
#include <QPair>
#include <QStack>
#include <QSet>
#include <QRandomGenerator>
#include <functional>
#include "mainwindow.h"
#include "squareitem.h"


class MainWindow;
class GameBoard:public QObject
{
    Q_OBJECT
    public:
        GameBoard(QFrame * main_frame=0, int p_lines=0, int p_columns=0, int p_height=0, int p_length=0, int p_number_of_bombs=0, int p_fontSize =12);
        ~GameBoard();
        inline int getSize()const{return buttons.size();}
    public slots:
        void generateBombs(int x_pos, int y_pos);
        void processButton(int x_pos,int y_pos);
        void gameOver(int x_pos, int y_pos);
        inline int getLines()const{return lines;}
        inline int getColumns()const{return columns;}
        inline int getHeight()const{return height;}
        inline int getLength()const{return length;}
        inline int getNumberOfBombs()const{return NumberOfBombs;}
        inline int getFontSize()const{return fontSize;}

    signals:
        void gameWon();
        void signalIsMarkedAsABomb(bool);
        void firstClick();
        void signalGameOver();

    private:
        QVector<QVector<QPointer<SquareItem>>> buttons;
        void assignNumbersToSquares();
        QVector<QPair<int,int>> getNeighbours(int abcissa, int ordinate);
        int countBombsNeighbours(QVector<QPair<int,int>> & neighbours_);
        void freeCells(int x_pos, int y_pos);
        QVector<QPair<int,int>> indexOfBombs;
        void disableAllButtons();
        QVector<QPair<int,int>> allIndexes;
        void checkIfWin();
        void revealMistakes();
        int lines;
        int columns;
        int height;
        int length;
        int NumberOfBombs;
        int fontSize;
};

#endif // GAMEBOARD_H
