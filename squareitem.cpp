#include "squareitem.h"


bool SquareItem::firstClick = true;
SquareItem::SquareItem(QWidget *parent):QPushButton(parent)
{

}

SquareItem::~SquareItem()
{
    SquareItem::firstClick = true;
}

SquareItem::SquareItem(int p_xpos, int p_ypos, int p_height, int p_length, bool p_isbomb, int p_associatedBombs, bool p_isMarkedAsABomb, QWidget *parent, int fontSize):
    QPushButton(parent), isBomb(p_isbomb), associatedBombs(p_associatedBombs),
    isMarkedAsABomb(p_isMarkedAsABomb),xpos(p_xpos/p_length),ypos(p_ypos/p_height),
    mFontSize(fontSize)
{
    setGeometry(p_xpos, p_ypos, p_height, p_length);
    if((xpos+ypos)%2==0)
    {
        setStyleSheet(":enabled{background-color: rgb(128, 119, 218);border-color: rgb(0, 0, 0);}"
                      ":disabled {background-color: rgb(128, 119, 218 );border-color: rgb(0, 0, 0);}");//237 * 3
    }
    else
    {
        setStyleSheet(":enabled{background-color: rgb(90, 79, 207);border-color: rgb(0, 0, 0);}"
                      ":disabled{background-color: rgb(90, 79, 207);border-color: rgb(0, 0, 0);}");
    }
}

SquareItem::SquareItem(int p_xpos, int p_ypos, int p_height, int p_length, QWidget *parent, int fontSize):
    SquareItem(p_xpos, p_ypos, p_height, p_length, false, 0, false, parent, fontSize)
{

}

void SquareItem::mousePressEvent(QMouseEvent *e)
{
    if(e->button()==Qt::LeftButton && !isMarkedAsABomb)
    {
        if(SquareItem::firstClick)
        {
            emit signalfirstClick(xpos, ypos);
            SquareItem::firstClick = false;
        }
        else
        {
            emit buttonClicked(xpos, ypos);
        }
    }
    else if(e->button() == Qt::RightButton)
    {
        setIsMarKedAsABomb();
    }
}

void SquareItem::showButton()
{
    if(isBomb)
    {
        QIcon icon;
        icon.addPixmap(QPixmap(":/icons/icons/explosion.png"), QIcon::Disabled);
        setIcon(icon);
        emit bombPressed(xpos, ypos);
    }
    else
    {
        if(associatedBombs!=0)
        {
            setText(QString::number(associatedBombs));
        }
        if((xpos+ypos)%2==0)
        {
           setStyleSheet("border-color: rgb(0, 0, 0);background-color: rgb(240, 234, 214);"+ getAssociatedColor()+"font:"+ QString::number(mFontSize) + "pt \"MS Shell Dlg 2\";");
        }
        else
        {
            setStyleSheet("border-color: rgb(0, 0, 0);background-color: rgb(226, 220, 201);"+getAssociatedColor()+"font:"+QString::number(mFontSize)+"pt \"MS Shell Dlg 2\";");
        }
        if(isMarkedAsABomb)
        {
            setIsMarKedAsABomb();
        }
        setEnabled(false);
    }
}

void SquareItem::setAsBomb()
{
    isBomb = true;
}

void SquareItem::setAssociatedBombs(int p_associatedBombs)
{
    associatedBombs = p_associatedBombs;
}

void SquareItem::setIsMarKedAsABomb()
{
    isMarkedAsABomb = ! isMarkedAsABomb;
    changeMarkedIcon();
    emit signalIsMarkedAsABomb(isMarkedAsABomb);
}

void SquareItem::changeMarkedIcon()
{
    if(isMarkedAsABomb)
    {
        QIcon icon;
        icon.addPixmap(QPixmap(":/icons/icons/flag.png"), QIcon::Disabled);
        setIcon(icon);
    }
    else
    {
        setIcon(QIcon());
    }
}


QString SquareItem::getAssociatedColor()
{
    switch(associatedBombs)
    {
        case 1:
            return QString("color: rgb(17, 52, 110);");
            break;
        case 2:
            return QString("color: rgb(40, 178, 10);");
            break;
        case 3:
            return QString("color: rgb(255, 58, 117);");
            break;
        case 4:
            return QString("color: rgb(240, 23, 255);");
            break;
        case 5:
            return QString("color: rgb(76, 70, 50);");
            break;
        case 6:
            return QString("color: rgb(123, 147, 255);");
            break;
        case 7:
            return QString("color: rgb(129, 126, 126);");
            break;
        case 8:
            return QString("color: rgb(0, 0, 0);");
            break;
        default:
            return QString("");
    }

}
