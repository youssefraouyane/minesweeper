#include "toolbar.h"

ToolBar::ToolBar(QWidget * parent):
    QToolBar(parent)
{
    initializeActions();
    addActionsToToolbar();
    setMovable(false);
    setFloatable(false);
}

ToolBar::~ToolBar()
{

}

void ToolBar::initializeActions()
{
    QIcon reset_icon;
    reset_icon.addPixmap(QPixmap(":/icons/icons/reset.png"));
    mReset = new QAction(reset_icon, "", this);

    mSkullFace = new QLabel(this);
    QPixmap mPixmap_1(":/icons/icons/skull.png");
    mSkullFace->setPixmap(mPixmap_1.scaled(50,50,Qt::KeepAspectRatio));

    mRemainingBombs = new QLabel(this);
    mRemainingBombs->setStyleSheet("font: 81 15pt \"Rockwell Extra Bold\";");
    mRemainingBombs->setText("40");


    mClock = new QLabel(this);
    QPixmap mPixmap_2(":/icons/icons/alarm-clock.png");
    mClock->setPixmap(mPixmap_2.scaled(50,50,Qt::KeepAspectRatio));

    mTimer = new TimerLabel(this);

}

void ToolBar::addActionsToToolbar()
{
    addAction(mReset);

    QWidget* placeholder_1 = new QWidget(this);
    insertWidget(new QAction(this), placeholder_1);
    placeholder_1->setMinimumWidth(200);

    addWidget(mSkullFace);
    addWidget(mRemainingBombs);

    QWidget* placeholder_2 = new QWidget(this);
    insertWidget(new QAction(this), placeholder_2);
    placeholder_2->setMinimumWidth(50);

    addWidget(mClock);
    addWidget(mTimer);

}

void ToolBar::updateRemainingBombs(bool isMarked)
{
    if(isMarked)
        mRemainingBombs->setText(QString::number(mRemainingBombs->text().toInt()-1));
    else
        mRemainingBombs->setText(QString::number(mRemainingBombs->text().toInt()+1));

}

