#include "gameboard.h"

GameBoard::GameBoard(QFrame * main_frame, int p_lines, int p_columns, int p_height, int p_length, int p_number_of_bombs, int p_fontSize):
    lines(p_lines),columns(p_columns), height(p_height), length(p_length), NumberOfBombs(p_number_of_bombs), fontSize(p_fontSize)
{
    for(int i=0;i<lines;i++)
    {
        QVector<QPointer<SquareItem>> lineItem;
        for(int j=0;j<columns;j++)
        {
            QPointer<SquareItem> currentSquare = new SquareItem(i*height,j*length, height, length, main_frame, fontSize);
            lineItem.push_back(currentSquare);
            connect(currentSquare, SIGNAL(signalfirstClick(int, int)), this, SLOT(generateBombs(int,int)));
            connect(currentSquare, SIGNAL(signalfirstClick(int, int)), this, SLOT(processButton(int,int)));
            connect(currentSquare, SIGNAL(buttonClicked(int, int)), this, SLOT(processButton(int,int)));
            connect(currentSquare, SIGNAL(bombPressed(int, int)), this, SLOT(gameOver(int, int)));
            connect(currentSquare, SIGNAL(signalIsMarkedAsABomb(bool)), this, SIGNAL(signalIsMarkedAsABomb(bool)));
            allIndexes.push_back(qMakePair(i,j));
        }
        buttons.push_back(lineItem);
    }
}

GameBoard::~GameBoard()
{

}

void GameBoard::generateBombs(int x_pos, int y_pos)
{
    int n = buttons.size();
    int m = buttons[0].size();
    QVector<QPair<int,int>> neighbours = getNeighbours(x_pos, y_pos);
    neighbours.append(qMakePair(x_pos,y_pos));
    emit firstClick();
    while(NumberOfBombs>0)
    {
        bool repeat = true;
        while(repeat)
        {
            int x = QRandomGenerator::global()->bounded(0, n);
            int y = QRandomGenerator::global()->bounded(0, m);
            if(!neighbours.contains(qMakePair(x,y)))
            {
                if(!buttons[x][y]->getIsBomb())
                {
                    buttons[x][y]->setAsBomb();
                    buttons[x][y]->setAssociatedBombs(-1);
                    indexOfBombs.push_back(qMakePair(x,y));
                    allIndexes.removeOne(qMakePair(x,y));
                    repeat = false;
                }
            }
        }
        NumberOfBombs--;
    }
    NumberOfBombs = indexOfBombs.size();
    assignNumbersToSquares();
}

void GameBoard::processButton(int x_pos,int y_pos)
{
    buttons[x_pos][y_pos]->showButton();
    freeCells(x_pos,y_pos);
    QPair<int,int> currentIndex(x_pos,y_pos);
    if(!indexOfBombs.contains(currentIndex))
    {
        allIndexes.removeOne(currentIndex);
        checkIfWin();
    }
}

void GameBoard::gameOver(int x_pos, int y_pos)
{
    QPair<int, int> bombPressed = qMakePair(x_pos, y_pos);
    indexOfBombs.removeOne(bombPressed);
    QIcon icon;
    icon.addPixmap(QPixmap(":/icons/icons/nuclear-explosion.png"), QIcon::Disabled);
    for(int i=0;i<indexOfBombs.size();i++)
    {
        int x_ = indexOfBombs[i].first;
        int y_ = indexOfBombs[i].second;
        if(!buttons[x_][y_]->getIsMarkedAsBomb())
        {
            buttons[x_][y_]->setIcon(icon);
        }
    }
    disableAllButtons();
    revealMistakes();
    emit signalGameOver();
}

void GameBoard::assignNumbersToSquares()
{
    int n = buttons.size();
    int m = buttons[0].size();
    for(int i = 0;i<n;i++)
    {
        for(int j=0;j<m;j++)
        {
            if(!buttons[i][j]->getIsBomb())
            {
                QVector<QPair<int,int>> neighbours = getNeighbours(i,j);
                buttons[i][j]->setAssociatedBombs(countBombsNeighbours(neighbours));
            }
        }
    }

}

QVector<QPair<int,int>> GameBoard::getNeighbours(int abcissa, int ordinate)
{
    QVector<QPair<int,int>> neighbours;

    if(abcissa == 0 && ordinate == 0)
    {
         neighbours.push_back(qMakePair(0,1));
         neighbours.push_back(qMakePair(1,1));
         neighbours.push_back(qMakePair(1,1));
    }
    else if(abcissa == 0 && ordinate == buttons[0].size()-1)
    {
        neighbours.push_back(qMakePair(1,buttons[0].size()-1));
        neighbours.push_back(qMakePair(1,buttons[0].size()-2));
        neighbours.push_back(qMakePair(0,buttons[0].size()-2));
    }
    else if(abcissa == buttons.size()-1 && ordinate ==0)
    {
        neighbours.push_back(qMakePair(buttons.size()-2,0));
        neighbours.push_back(qMakePair(buttons.size()-2,1));
        neighbours.push_back(qMakePair(buttons.size()-1,1));
    }
    else if(abcissa == buttons.size()-1 && ordinate == buttons[0].size()-1)
    {
        neighbours.push_back(qMakePair(buttons.size()-2,buttons[0].size()-1));
        neighbours.push_back(qMakePair(buttons.size()-2,buttons[0].size()-2));
        neighbours.push_back(qMakePair(buttons.size()-1,buttons[0].size()-2));
    }
    else if(abcissa ==0 && ordinate!= 0 && ordinate != buttons[0].size()-1)
    {
        neighbours.push_back(qMakePair(abcissa,ordinate+1));
        neighbours.push_back(qMakePair(abcissa,ordinate-1));
        neighbours.push_back(qMakePair(abcissa+1,ordinate));
        neighbours.push_back(qMakePair(abcissa+1,ordinate+1));
        neighbours.push_back(qMakePair(abcissa+1,ordinate-1));
    }
    else if(abcissa == buttons.size()-1 && ordinate !=0 && ordinate != buttons[0].size()-1)
    {
        neighbours.push_back(qMakePair(abcissa,ordinate+1));
        neighbours.push_back(qMakePair(abcissa,ordinate-1));
        neighbours.push_back(qMakePair(abcissa-1,ordinate));
        neighbours.push_back(qMakePair(abcissa-1,ordinate+1));
        neighbours.push_back(qMakePair(abcissa-1,ordinate-1));
    }
    else if(abcissa != 0 && abcissa != buttons.size()-1 && ordinate ==0)
    {
        neighbours.push_back(qMakePair(abcissa+1,ordinate));
        neighbours.push_back(qMakePair(abcissa-1,ordinate));
        neighbours.push_back(qMakePair(abcissa+1,ordinate+1));
        neighbours.push_back(qMakePair(abcissa,ordinate+1));
        neighbours.push_back(qMakePair(abcissa-1,ordinate+1));
    }
    else if(abcissa!=0 && abcissa !=buttons.size()-1 && ordinate == buttons[0].size()-1)
    {
        neighbours.push_back(qMakePair(abcissa+1,ordinate));
        neighbours.push_back(qMakePair(abcissa-1,ordinate));
        neighbours.push_back(qMakePair(abcissa+1,ordinate-1));
        neighbours.push_back(qMakePair(abcissa,ordinate-1));
        neighbours.push_back(qMakePair(abcissa-1,ordinate-1));
    }
    else
    {
        neighbours.push_back(qMakePair(abcissa+1,ordinate));
        neighbours.push_back(qMakePair(abcissa+1,ordinate-1));
        neighbours.push_back(qMakePair(abcissa+1,ordinate+1));
        neighbours.push_back(qMakePair(abcissa,ordinate+1));
        neighbours.push_back(qMakePair(abcissa,ordinate-1));
        neighbours.push_back(qMakePair(abcissa-1,ordinate+1));
        neighbours.push_back(qMakePair(abcissa-1,ordinate));
        neighbours.push_back(qMakePair(abcissa-1,ordinate-1));
    }
    return neighbours;
}


int GameBoard::countBombsNeighbours(QVector<QPair<int,int>> & neighbours_)
{
    int bombsCounted = 0;
    for(int i=0;i<neighbours_.size();i++)
    {
        if(buttons[neighbours_[i].first][neighbours_[i].second]->getIsBomb())
        {
            bombsCounted++;
        }
    }
    return bombsCounted;
}


void GameBoard::freeCells(int x_pos, int y_pos)
{
    QStack<QPair<int,int>> cellsToShow;
    QSet<QPair<int,int>> visitedCells;
    cellsToShow.push(qMakePair(x_pos,y_pos));
    while(!cellsToShow.empty())
    {
        QPair<int,int> currentCell = cellsToShow.pop();
        int x = currentCell.first;
        int y = currentCell.second;
        visitedCells.insert(currentCell);
        buttons[x][y]->showButton();
        allIndexes.removeOne(qMakePair(x,y));
        if(buttons[x][y]->getAssociatedBombs()==0)
        {
            QVector<QPair<int,int>> neighbours = getNeighbours(x,y);
            for(int i=0;i<neighbours.size();i++)
            {
                if(!visitedCells.contains(neighbours[i]))
                {
                    cellsToShow.push(neighbours[i]);
                }
            }
        }
    }
}


void GameBoard::disableAllButtons()
{
    for(int i=0;i<buttons.size();i++)
    {
        for(int j=0;j<buttons[0].size();j++)
        {
            buttons[i][j]->setEnabled(false);
        }
    }
}

void GameBoard::checkIfWin()
{
    if(allIndexes.size()==0)
    {
        disableAllButtons();
        emit gameWon();
    }
}


void GameBoard::revealMistakes()
{
    for(int i=0;i<allIndexes.size();i++)
    {
        if(buttons[allIndexes[i].first][allIndexes[i].second]->getIsMarkedAsBomb() && !buttons[allIndexes[i].first][allIndexes[i].second]->getIsBomb())
        {
            QIcon icon;
            icon.addPixmap(QPixmap(":/icons/trollface.png"), QIcon::Disabled);
            buttons[allIndexes[i].first][allIndexes[i].second]->setIcon(icon);
        }
    }
}
