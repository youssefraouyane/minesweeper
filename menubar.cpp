#include "menubar.h"

MenuBar::MenuBar(QWidget * parent): QMenuBar(parent)
{
    initializeActions();
    addActionsToTheMenu();
}

MenuBar::~MenuBar()
{

}
void MenuBar::initializeActions()
{
    generalMenu = new QMenu("General", this);
    mDifficultiesMenu = new QMenu("Difficulty");
    newGame = new QAction("New Game", generalMenu);
    newGame->setShortcut(Qt::CTRL + Qt::Key_N);
    newGame->setShortcutVisibleInContextMenu(true);

    hallOfFame = new QAction("Hall of Fame", generalMenu);
    hallOfFame->setShortcut(Qt::CTRL + Qt::Key_H);
    hallOfFame->setShortcutVisibleInContextMenu(true);

    mDifficultiesActionGroup = new QActionGroup(this);

    easyMode = new QAction("Easy", mDifficultiesActionGroup);
    easyMode->setCheckable(true);
    mediumMode = new QAction("Medium", mDifficultiesActionGroup);
    mediumMode->setCheckable(true);
    mediumMode->setChecked(true);
    hardMode = new QAction("Hard", mDifficultiesActionGroup);
    hardMode->setCheckable(true);


    closeGame = new QAction("Close Game", generalMenu);
    closeGame->setShortcut(Qt::ALT + Qt::Key_F4);
    closeGame->setShortcutVisibleInContextMenu(true);
}

void MenuBar::addActionsToTheMenu()
{
    generalMenu->addAction(newGame);
    generalMenu->addAction(hallOfFame);
    generalMenu->addSeparator();
    mDifficultiesMenu->addAction(easyMode);
    mDifficultiesMenu->addAction(mediumMode);
    mDifficultiesMenu->addAction(hardMode);
    generalMenu->addMenu(mDifficultiesMenu);
    generalMenu->addSeparator();
    generalMenu->addAction(closeGame);
    addMenu(generalMenu);
}
