#ifndef TIMERLABEL_H
#define TIMERLABEL_H
#include <QLabel>
#include <QTimer>

class TimerLabel:public QLabel
{
    Q_OBJECT
    public:
        TimerLabel(QWidget * parent = nullptr);
        ~TimerLabel();

    public slots:
        void resetTimer();
        void startTimer();
        void stopTimer();

    private slots:
        void incrementCounter();
    private:
        int counter;
        QTimer * mTimer;

};

#endif // TIMERLABEL_H
