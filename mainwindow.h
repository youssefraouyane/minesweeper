#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMetaEnum>
#include <QString>
#include <QPointer>
#include <QPushButton>
#include <QFrame>
#include "gameboard.h"
#include "menubar.h"
#include "toolbar.h"

class GameBoard;
class MenuBar;
class MainWindow:public QMainWindow
{
    Q_OBJECT
    public:
        Q_ENUMS(LEVELS);
        MainWindow(QWidget * parent = nullptr);
        ~MainWindow();
        enum LEVELS
        {
            EASY, MEDIUM, HARD
        };
    public slots:
        void setLevel(QAction * p_current_action);
        void slotResetTheGame(bool reset_clicked);
    private:
        QPointer<GameBoard> mGameBoard;
        QPointer<MenuBar> mMenuBar;
        QPointer<QFrame> mFrame;
        QPointer<ToolBar> mToolbar;
        LEVELS mCurrentLevel;

        void setGeneralConnections();
        void setGameBoardConnections();
        void reallocateForNewDifficulty(int lines, int columns, int height, int length, int numberOfBombs, int fontSize);
        void initializeToolBarActions(int numberOfBombs);

};
#endif // MAINWINDOW_H
