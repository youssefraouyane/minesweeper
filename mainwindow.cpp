#include "mainwindow.h"


MainWindow::MainWindow(QWidget * parent):
    QMainWindow(parent),mCurrentLevel(MEDIUM)
{
    setWindowIcon(QIcon(":/icons/icons/explosion.png"));
    setContextMenuPolicy(Qt::NoContextMenu);
    mMenuBar = new MenuBar(this);
    setMenuBar(mMenuBar);

    mFrame = new QFrame(this);
    mFrame->setFrameShape(QFrame::NoFrame);
    setCentralWidget(mFrame);
    mGameBoard = new GameBoard(mFrame, 18,14,50,50,40,15);
    //resize(24*40,);
    mToolbar = new ToolBar(this);
    addToolBar(mToolbar);
    setGeneralConnections();
    initializeToolBarActions(40);
    setGameBoardConnections();
}

MainWindow::~MainWindow()
{
    delete mGameBoard;
}



void MainWindow::setLevel(QAction * p_current_action)
{
   QMetaObject metaObject = this->staticMetaObject;
   QMetaEnum metaEnum = metaObject.enumerator(metaObject.indexOfEnumerator("LEVELS"));
   QString new_difficulty(p_current_action->text());
   if(mCurrentLevel != metaEnum.keysToValue(new_difficulty.toUpper().toLatin1()))
   {
      switch(metaEnum.keysToValue(new_difficulty.toUpper().toLatin1()))
      {
          case EASY:
              reallocateForNewDifficulty(8,10,60,60,10,20);
              mCurrentLevel = EASY;
              break;
          case MEDIUM:
              reallocateForNewDifficulty(18,14,50,50,40,15);
              mCurrentLevel = MEDIUM;
              break;
          case HARD:
              reallocateForNewDifficulty(20,24,35,35,99,12);
              mCurrentLevel = HARD;
              break;
          default:
              break;
      }
   }

}

void MainWindow::slotResetTheGame(bool reset_clicked)
{
    if(!reset_clicked)
    {
        reallocateForNewDifficulty(mGameBoard->getLines(),mGameBoard->getColumns(), mGameBoard->getHeight(),
                                   mGameBoard->getLength(), mGameBoard->getNumberOfBombs(), mGameBoard->getFontSize());
    }
}

void MainWindow::setGeneralConnections()
{
    connect(mMenuBar->getDifficultiesActionGroup(), SIGNAL(triggered(QAction *)), this, SLOT(setLevel(QAction *)));
    connect(mMenuBar->getNewGameAction(), SIGNAL(triggered(bool)),mToolbar->getResetAction(), SIGNAL(triggered(bool)));
    connect(mToolbar->getResetAction(), SIGNAL(triggered(bool)), this, SLOT(slotResetTheGame(bool)));
    connect(mMenuBar->getCloseGameAction(), SIGNAL(triggered(bool)), this, SLOT(close()));
    //Hall Of Fame
}

void MainWindow::setGameBoardConnections()
{
    connect(mGameBoard, SIGNAL(signalIsMarkedAsABomb(bool)), mToolbar, SLOT(updateRemainingBombs(bool)));
    connect(mGameBoard, SIGNAL(firstClick()), mToolbar->getTimerLabel(), SLOT(startTimer()));
    connect(mGameBoard, SIGNAL(signalGameOver()), mToolbar->getTimerLabel(), SLOT(stopTimer()));
    connect(mGameBoard, SIGNAL(gameWon()), mToolbar->getTimerLabel(), SLOT(stopTimer()));
}

void MainWindow::reallocateForNewDifficulty(int lines, int columns, int height, int length, int numberOfBombs, int fontSize)
{
    mToolbar->getTimerLabel()->resetTimer();
    QPointer <QFrame> temporary_frame_pointer = new QFrame(this);
    temporary_frame_pointer->setFrameShape(QFrame::NoFrame);
    QPointer<GameBoard> temporary_game_board_pointer = new GameBoard(temporary_frame_pointer, lines, columns, height,
                                                                     length, numberOfBombs, fontSize);
    temporary_frame_pointer.swap(mFrame);
    temporary_game_board_pointer.swap(mGameBoard);
    setCentralWidget(mFrame);
    initializeToolBarActions(mGameBoard->getNumberOfBombs());
    setGameBoardConnections();
    delete temporary_frame_pointer;
    delete temporary_game_board_pointer;
}

void MainWindow::initializeToolBarActions(int numberOfBombs)
{
    mToolbar->getRemainingBombsLabel()->setText(QString::number(numberOfBombs));
}
